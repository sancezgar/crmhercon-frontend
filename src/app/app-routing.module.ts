import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { SucursalesComponent } from './components/sucursales/sucursales.component';
import { CreaSucursalComponent } from './components/crea-sucursal/crea-sucursal.component';
import { EditaSucursalComponent } from './components/edita-sucursal/edita-sucursal.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { CrearUsuarioComponent } from './components/crear-usuario/crear-usuario.component';
import { EditaUsuarioComponent } from './components/edita-usuario/edita-usuario.component';
import { AccionComponent } from './components/accion/accion.component';
import { CrearAccionComponent } from './components/crear-accion/crear-accion.component';
import { EditarAccionComponent } from './components/editar-accion/editar-accion.component';
import { SectorEconomicoComponent } from './components/sector-economico/sector-economico.component';
import { CreaSectorEconomicoComponent } from './components/crea-sector-economico/crea-sector-economico.component';
import { EditaSectorEconomicoComponent } from './components/edita-sector-economico/edita-sector-economico.component';


const routes: Routes = [
  {path: 'inicio', component: InicioComponent},
  {path: 'inicio/:sure', component: InicioComponent}, // Cerrar Sesión
  {path: 'sucursales', component: SucursalesComponent},
  {path: 'crearSucursal', component: CreaSucursalComponent},
  {path: 'editaSucursal/:id', component: EditaSucursalComponent},
  {path: 'usuarios', component: UsuariosComponent},
  {path: 'crearUsuario', component: CrearUsuarioComponent},
  {path: 'editaUsuario/:id', component: EditaUsuarioComponent},
  {path: 'accion', component: AccionComponent},
  {path: 'creaAccion', component: CrearAccionComponent},
  {path: 'editarAccion/:id', component: EditarAccionComponent},
  {path: 'sector-economico', component: SectorEconomicoComponent},
  {path: 'creaSector', component: CreaSectorEconomicoComponent},
  {path: 'editaSector/:id', component: EditaSectorEconomicoComponent},
  {path: '**', redirectTo: '/inicio'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
