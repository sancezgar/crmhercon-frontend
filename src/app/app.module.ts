import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { HeaderComponent } from './components/header/header.component';
import { SucursalesComponent } from './components/sucursales/sucursales.component';
import { LoginComponent } from './components/login/login.component';
import { CreaSucursalComponent } from './components/crea-sucursal/crea-sucursal.component';
import { EditaSucursalComponent } from './components/edita-sucursal/edita-sucursal.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';

// prime
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { CrearUsuarioComponent } from './components/crear-usuario/crear-usuario.component';
import { EditaUsuarioComponent } from './components/edita-usuario/edita-usuario.component';
import { AccionComponent } from './components/accion/accion.component';
import { CrearAccionComponent } from './components/crear-accion/crear-accion.component';
import { EditarAccionComponent } from './components/editar-accion/editar-accion.component';
import { SectorEconomicoComponent } from './components/sector-economico/sector-economico.component';
import { CreaSectorEconomicoComponent } from './components/crea-sector-economico/crea-sector-economico.component';
import { EditaSectorEconomicoComponent } from './components/edita-sector-economico/edita-sector-economico.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    InicioComponent,
    HeaderComponent,
    SucursalesComponent,
    LoginComponent,
    CreaSucursalComponent,
    EditaSucursalComponent,
    UsuariosComponent,
    CrearUsuarioComponent,
    EditaUsuarioComponent,
    AccionComponent,
    CrearAccionComponent,
    EditarAccionComponent,
    SectorEconomicoComponent,
    CreaSectorEconomicoComponent,
    EditaSectorEconomicoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ProgressSpinnerModule,
    MatProgressSpinnerModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
