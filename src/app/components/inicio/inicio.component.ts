import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  public title: string;
  public breadcum: Array<string>;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {

    this.title = 'Inicio';
    this.breadcum = ['Inicio'];

   }

  ngOnInit(): void {

    this.route.params.subscribe(
      params => {

        if (params.sure){
          localStorage.removeItem('token');
          localStorage.removeItem('identity');
          this.router.navigate(['/inicio']);
        }

      }
    );

  }

}
