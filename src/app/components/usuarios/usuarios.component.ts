import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { UsuariosModel } from '../../models/usuarios.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css'],
  providers: [UsuarioService, UsuariosModel]
})
export class UsuariosComponent implements OnInit {

  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  public token: string;
  public usuarios: Array<UsuariosModel>;
  public status: string;
  public mensaje: string;

  constructor(
    private usuarioService: UsuarioService,
    private _snackBar: MatSnackBar
  ) {

    this.title = 'Página de Usuarios';
    this.breadcum = ['Inicio', 'Usuarios'];
    this.rutaBreadcum = '/inicio';
    this.token = this.usuarioService.getToken();

   }

  ngOnInit(): void {
    this.getUsuarios();
  }

  getUsuarios(){
    this.usuarioService.getUsuarios(this.token).subscribe(
      response => {
        if (response.status === 'success'){
          this.usuarios = response.users;
        }else{
          console.log(response);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  eliminarUsuario(id: number){
    if (confirm('¿Deseas ELIMINAR este registro?')){
      this.usuarioService.eliminaUsuario(this.token, id).subscribe(
        response => {
          if (response.status === 'success'){
            console.log(response);
            this.status = 'success';
            this.mensaje = response.message;
            this.getUsuarios();
            this.openSnackBar(this.mensaje);
          }
        },
        error => {
          console.log(error);
          this.status = 'error';
          this.mensaje = error.error.message;
          this.getUsuarios();
          this.openSnackBar(this.mensaje);
        }
      );

    }
  }

  openSnackBar(mensaje: string) {
    this._snackBar.open(mensaje, 'aceptar', {
      duration: 5000,
    });
  }

}
