import { Component, OnInit } from '@angular/core';
import { SucursalesModel } from '../../models/sucursales.model';
import { SucursalService } from '../../services/sucursal.service';
import { UsuarioService } from '../../services/usuario.service';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edita-sucursal',
  templateUrl: '../crea-sucursal/crea-sucursal.component.html',
  styleUrls: ['./edita-sucursal.component.css']
})
export class EditaSucursalComponent implements OnInit {

  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  public sucursal: SucursalesModel;
  public status: string;
  public mensaje: string;
  private token: string;

  constructor(
    private sucursalService: SucursalService,
    private usuarioService: UsuarioService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.title = 'Edita Sucursal';
    this.breadcum = ['Sucursal', 'edita-sucursal'];
    this.rutaBreadcum = '/sucursales';
    this.mensaje = '';
    this.sucursal = new SucursalesModel();
    this.token = this.usuarioService.getToken();
  }

  ngOnInit(): void {

    this.route.params.subscribe(
      params =>  {
        const id = params.id;
        this.sucursalService.getSucursal(this.token, id).subscribe(
          response => {
            if (response.status === 'success'){
              this.sucursal = response.sucursal;
            }
          },
          error => {
            console.log(error);
          }
        );
      }
    );

  }

  onSubmit(form: NgForm){
    if ( form.invalid ) { return; }

    this.sucursalService.editarSucursal(this.sucursal, this.token, this.sucursal.id_sucursal).subscribe(
      /* ================================================== *
       * ==========  La respuesta es correcta  ========== *
       * ================================================== */
      response => {
        if (response.status === 'success'){
          this.status = 'success';
          this.mensaje = response.message;
          this.router.navigate(['/sucursales']);
        }else{
          console.log(response);
          this.status = 'error';
          this.mensaje = response.message;
        }

      /* =======  End of La respuesta es correcta  ======= */
      },

      /* ================================================== *
       * ==========  La respuesta no es correcta  ========== *
       * ================================================== */

      error => {
        console.log(error);
        this.status = 'error';

        if ( error.error.errors !== undefined ){
          const errs = error.error.errors;
          // tslint:disable-next-line: forin
          for ( const errKey in errs){
            this.mensaje += errs[errKey] + '<br>';
          }
        }
        else{
          this.mensaje = error.error.message;
        }
      }

      /* =======  End of La respuesta no es correcta  ======= */
    );

  }

}
