import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { SectorModel } from '../../models/sector.model';
import { SectorService } from '../../services/sector.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-sector-economico',
  templateUrl: './sector-economico.component.html',
  styleUrls: ['./sector-economico.component.css'],
  providers: [SectorModel]
})
export class SectorEconomicoComponent implements OnInit {
  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  private token: string;
  public status: string;
  public mensaje: string;
  public sectores: Array<SectorModel>;

  constructor(
    private usuarioService: UsuarioService,
    private sectorService: SectorService,
    // tslint:disable-next-line: variable-name
    private _snackBar: MatSnackBar
  ) {

    this.title = 'Página de Sectores Económicos';
    this.breadcum = ['Inicio', 'Sector Económico'];
    this.rutaBreadcum = '/inicio';
    this.token = this.usuarioService.getToken();
    this.getSectores();

   }

  ngOnInit(): void {
  }

  getSectores(){

    this.sectorService.getSectores(this.token).subscribe(

      response => {
        this.sectores = response.sectores;
      },

      error => {
        console.log(error);
      }

    );

  }

  eliminarSector(id: number){

    if (confirm('¿Deseas ELIMINAR el registro?')){

      this.sectorService.eliminaSector(this.token, id).subscribe(

        response => {
          this.status = 'success';
          this.mensaje = response.message;
          this.getSectores();
          this.openSnackBar(this.mensaje);
        },
        error => {
          console.log(error);
          this.status = 'error';
          this.mensaje = error.error.message;
          this.getSectores();
          this.openSnackBar(this.mensaje);
        }

      );

    }

  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'aceptar', {
      duration: 5000,
    });
  }

}
