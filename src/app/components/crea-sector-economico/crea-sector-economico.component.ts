import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { NgForm } from '@angular/forms';
import { SectorModel } from '../../models/sector.model';
import { SectorService } from '../../services/sector.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crea-sector-economico',
  templateUrl: './crea-sector-economico.component.html',
  styleUrls: ['./crea-sector-economico.component.css'],
  providers: [SectorModel]
})
export class CreaSectorEconomicoComponent implements OnInit {

  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  public token: string;
  public status: string;
  public mensaje: string;

  constructor(
    private usuarioService: UsuarioService,
    public sector: SectorModel,
    private sectorService: SectorService,
    private router: Router
  ) {

    this.title = 'Crea Sector Económico';
    this.rutaBreadcum = '/sector-economico';
    this.breadcum = ['Sector', 'Crea Sector'];
    this.token = this.usuarioService.getToken();

  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm){

    if (form.invalid) {return; }

    this.sectorService.creaSector(this.sector, this.token).subscribe(
      response => {
        this.status = 'success';
        this.mensaje = response.message;
        this.router.navigate(['/sector-economico']);
      },
      error => {
        console.log(error);
        this.status = 'error';
        this.mensaje = '';
        if (error.error.errors !== undefined){

          // tslint:disable-next-line: forin
          for (const errorkey in error.error.errors){

            this.mensaje += error.error.errors[errorkey] + '<br>';

          }

        }else{

          this.mensaje = error.error.message;

        }
      }
    );

  }

}
