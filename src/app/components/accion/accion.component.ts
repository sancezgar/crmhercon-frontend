import { Component, OnInit } from '@angular/core';
import { AccionesModel } from '../../models/acciones.model';
import { UsuarioService } from '../../services/usuario.service';
import { AccionService } from '../../services/accion.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-accion',
  templateUrl: './accion.component.html',
  styleUrls: ['./accion.component.css']
})
export class AccionComponent implements OnInit {
  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  private token: string;
  public acciones: Array<AccionesModel>;
  public mensaje: string;
  public status: string;
  constructor(
    private usuarioService: UsuarioService,
    private accionService: AccionService,
    // tslint:disable-next-line: variable-name
    private _snackBar: MatSnackBar
  ) {

    this.title = 'Acciones';
    this.breadcum = ['Inicio', 'Acciones'];
    this.rutaBreadcum = '/inicio';
    this.token = this.usuarioService.getToken();
    this.getAcciones();
   }

  ngOnInit(): void {
  }

  getAcciones(){

    this.accionService.getAcciones(this.token).subscribe(
      response => {
        if (response.status === 'success'){
          this.acciones = response.acciones;
        }else{
          console.log(response);
        }
      },
      error => {
        console.log(error);
      }
    );

  }

  eliminarAccion(id: number){

    if (confirm('¿Desea eliminar este registro?')){
      this.accionService.eliminaAccion(this.token, id).subscribe(
        response => {
          if (response.status === 'success'){
            this.status = 'success';
            this.mensaje = response.message;
            this.getAcciones();
            this.openSnackBar(this.mensaje);
          }
        },
        error => {
          console.log(error);
          this.status = 'error';
          this.mensaje = error.error.message;
          this.getAcciones();
          this.openSnackBar(this.mensaje);
        }
      );
    }

  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'aceptar', {
      duration: 5000,
    });
  }

}
