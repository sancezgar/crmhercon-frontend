import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { AccionesModel } from '../../models/acciones.model';
import { AccionService } from '../../services/accion.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-accion',
  templateUrl: './crear-accion.component.html',
  styleUrls: ['./crear-accion.component.css'],
  providers: [AccionesModel]
})
export class CrearAccionComponent implements OnInit {
  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  public status: string;
  public mensaje: string;
  private token: string;

  constructor(
    private usuarioService: UsuarioService,
    private accionService: AccionService,
    public accion: AccionesModel,
    private router: Router
  ) {
    this.title = 'Crear Acciones';
    this.breadcum = ['Acción', 'Crea Acción'];
    this.rutaBreadcum = '/accion';
    this.token = this.usuarioService.getToken();
   }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm){

    if (form.invalid) { return; }

    this.accionService.creaAccion(this.accion, this.token).subscribe(
      response => {
        this.status = 'success';
        this.mensaje = response.message;
        this.router.navigate(['/accion']);
      },
      error => {
        console.log(error);
        this.status = 'error';
        this.mensaje = '';
        if (error.error.errors === undefined){
          this.mensaje = error.error.message;
        }else{
          // tslint:disable-next-line: forin
          for (const errorKey in error.error.errors){
            this.mensaje += error.error.errors[errorKey] + '<br>';
          }
        }

      }
    );

  }

}
