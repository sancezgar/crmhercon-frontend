import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { AccionService } from '../../services/accion.service';
import { AccionesModel } from '../../models/acciones.model';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-editar-accion',
  templateUrl: '../crear-accion/crear-accion.component.html',
  styleUrls: ['./editar-accion.component.css'],
  providers: [AccionesModel]
})
export class EditarAccionComponent implements OnInit {

  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  public status: string;
  public mensaje: string;
  private token: string;

  constructor(
    private usuarioService: UsuarioService,
    private accionService: AccionService,
    public accion: AccionesModel,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.title = 'Crear Acciones';
    this.breadcum = ['Acción', 'Edita Acción'];
    this.rutaBreadcum = '/accion';
    this.token = this.usuarioService.getToken();
   }

  ngOnInit(): void {
    this.getAccion();
  }

  getAccion(){

    this.route.params.subscribe(
      params => {
        const id = params.id;
        this.accionService.getAccion(this.token, id).subscribe(
          response => {
            this.accion = response.accion;
          },
          error => {
            console.log(error);
            this.status = 'error';
            this.mensaje = error.error.message;
          }
        );
      }
    );

  }



  onSubmit(form: NgForm){

    if (form.invalid) { return; }

    this.accionService.editaAccion(this.accion, this.token, this.accion.id_accion).subscribe(
      response => {
        this.status = 'success';
        this.mensaje = response.message;
        this.router.navigate(['/accion']);
      },
      error => {
        console.log(error);
        this.status = 'error';
        this.mensaje = '';
        if (error.error.errors === undefined){
          this.mensaje = error.error.message;
        }else{
          // tslint:disable-next-line: forin
          for (const errorKey in error.error.errors){
            this.mensaje += error.error.errors[errorKey] + '<br>';
          }
        }

      }
    );

  }

}
