import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { SectorService } from '../../services/sector.service';
import { SectorModel } from '../../models/sector.model';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edita-sector-economico',
  templateUrl: '../crea-sector-economico/crea-sector-economico.component.html',
  styleUrls: ['./edita-sector-economico.component.css'],
  providers: [SectorModel]
})
export class EditaSectorEconomicoComponent implements OnInit {

  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  public status: string;
  public mensaje: string;
  private token: string;

  constructor(
    private usuarioService: UsuarioService,
    private sectorService: SectorService,
    public sector: SectorModel,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.title = 'Edita Sector';
    this.breadcum = ['Sector', 'Edita Sector'];
    this.rutaBreadcum = '/sector-economico';
    this.token = this.usuarioService.getToken();
   }

  ngOnInit(): void {
    this.getAccion();
  }

  getAccion(){

    this.route.params.subscribe(
      params => {
        const id = params.id;
        this.sectorService.getSector(this.token, id).subscribe(
          response => {
            this.sector = response.sector;
          },
          error => {
            console.log(error);
            this.status = 'error';
            this.mensaje = error.error.message;
          }
        );
      }
    );

  }



  onSubmit(form: NgForm){

    if (form.invalid) { return; }

    this.sectorService.editaSector(this.sector, this.token, this.sector.id_sector).subscribe(
      response => {
        this.status = 'success';
        this.mensaje = response.message;
        this.router.navigate(['/sector-economico']);
      },
      error => {
        console.log(error);
        this.status = 'error';
        this.mensaje = '';
        if (error.error.errors === undefined){
          this.mensaje = error.error.message;
        }else{
          // tslint:disable-next-line: forin
          for (const errorKey in error.error.errors){
            this.mensaje += error.error.errors[errorKey] + '<br>';
          }
        }

      }
    );

  }

}
