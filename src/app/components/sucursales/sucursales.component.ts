import { Component, OnInit } from '@angular/core';
import { SucursalesModel } from '../../models/sucursales.model';
import { SucursalService } from '../../services/sucursal.service';
import { UsuarioService } from '../../services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-sucursales',
  templateUrl: './sucursales.component.html',
  styleUrls: ['./sucursales.component.css'],
  providers: [UsuarioService, SucursalService, SucursalesModel]
})
export class SucursalesComponent implements OnInit {

  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  public sucursales: Array<SucursalesModel>;
  public token: string;
  public status: string;
  public mensaje: string;

  constructor(
    private sucursalServicio: SucursalService,
    private usuarioService: UsuarioService,
    private _snackBar: MatSnackBar
  ) {
    this.title = 'Página de Sucursales';
    this.breadcum = ['Inicio', 'Sucursales'];
    this.rutaBreadcum = '/inicio';
    this.token = this.usuarioService.getToken();
  }

  ngOnInit(): void {
    this.getSucursales();
  }

  getSucursales(){

    this.sucursalServicio.getSucursales(this.token).subscribe(
      response => {
        if (response.status === 'success'){
          this.sucursales = response.sucursales;
          console.log(this.sucursales);
        }else{
          console.log(response);
          this.status = 'error';
        }
      },
      error => {
        console.log(error);
        this.status = 'error';
      }
    );

  }

  eliminarSucursal(id: number){
    if (confirm('¿Estas seguro de eliminarlo?')){
      this.sucursalServicio.eliminarSucursal(this.token, id).subscribe(
        response => {
          if ( response.status === 'success' ){
            alert('Fué eliminado con exito ' + response.sucursal.razon_social);
            this.getSucursales();
          }
        },
        error => {
          console.log(error);
          alert('Ocurrio un error, intentelo más tarde');
        }
      );
    }
  }

}
