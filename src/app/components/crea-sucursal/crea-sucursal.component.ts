import { Component, OnInit } from '@angular/core';
import { SucursalesModel } from '../../models/sucursales.model';
import { NgForm } from '@angular/forms';
import { SucursalService } from '../../services/sucursal.service';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-crea-sucursal',
  templateUrl: './crea-sucursal.component.html',
  styleUrls: ['./crea-sucursal.component.css']
})
export class CreaSucursalComponent implements OnInit {

  public title: string;
  public breadcum: Array<any>;
  public rutaBreadcum: string;
  private token: string;
  public mensaje: string;
  public status: string;
  public sucursal: SucursalesModel;

  constructor(
    private sucursalService: SucursalService,
    private usuarioService: UsuarioService
  ) {
    this.title = 'Crear Sucursal';
    this.breadcum = ['Sucursal', 'crea-sucursal'];
    this.rutaBreadcum = '/sucursales';
    this.sucursal = new SucursalesModel();
    this.token = this.usuarioService.getToken();
    this.mensaje = '';
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm){
    if (form.invalid) { return; }

    this.sucursalService.crearSucursal(this.sucursal, this.token).subscribe(

      /* ================================================== *
       * ==========  La respuesta es correcta  ========== *
       * ================================================== */
      response => {
        if (response.status === 'success'){
          this.status = 'success';
          this.mensaje = response.message;
          form.reset();
        }else{
          console.log(response);
          this.status = 'error';
          this.mensaje = response.message;
        }

      /* =======  End of La respuesta es correcta  ======= */
      },

      /* ================================================== *
       * ==========  La respuesta no es correcta  ========== *
       * ================================================== */

      error => {
        console.log(error);
        this.status = 'error';

        if ( error.error.errors !== undefined ){
          const errs = error.error.errors;
          // tslint:disable-next-line: forin
          for ( const errKey in errs){
            this.mensaje += errs[errKey] + '<br>';
          }
        }
        else{
          this.mensaje = error.error.message;
        }
      }

      /* =======  End of La respuesta no es correcta  ======= */
    );
  }

}
