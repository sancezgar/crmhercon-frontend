import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { UsuariosModel } from '../../models/usuarios.model';
import { Router } from '@angular/router';
import { SucursalesModel } from '../../models/sucursales.model';
import { SucursalService } from '../../services/sucursal.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css'],
  providers: [UsuariosModel, SucursalesModel]
})
export class CrearUsuarioComponent implements OnInit {

  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  private token: string;
  public status: string;
  public mensaje: string;

  constructor(
    private router: Router,
    private usuarioService: UsuarioService,
    private sucursalService: SucursalService,
    public usuario: UsuariosModel,
    public sucursales: SucursalesModel
  ) {

    this.rutaBreadcum = '/usuarios';
    this.title = 'Crear Usuario';
    this.breadcum = ['Usuarios', 'crear-usuario'];
    this.token = this.usuarioService.getToken();
    this.getSucursales();

   }

  ngOnInit(): void {
  }

  getSucursales(){

    this.sucursalService.getSucursales(this.token).subscribe(
      response => {
        this.sucursales = response.sucursales;
      },
      error => {
        console.log(error);
      }
    );

  }

  onSubmit(form: NgForm){
    if (form.invalid) { return; }

    if (this.usuario.contrasena !== ''){

      this.usuarioService.crearUsuario(this.usuario, this.token).subscribe(
        /* ================================================== *
       * ==========  La respuesta es correcta  ========== *
       * ================================================== */
      response => {
        if (response.status === 'success'){
          this.status = 'success';
          this.mensaje = response.message;
          form.reset();
          this.router.navigate(['/usuarios']);
        }else{
          console.log(response);
          this.status = 'error';
          this.mensaje = response.message;
        }

      /* =======  End of La respuesta es correcta  ======= */
      },

      /* ================================================== *
       * ==========  La respuesta no es correcta  ========== *
       * ================================================== */

      error => {
        console.log(error);
        this.status = 'error';

        if ( error.error.errors !== undefined ){
          const errs = error.error.errors;
          // tslint:disable-next-line: forin
          for ( const errKey in errs){
            this.mensaje += errs[errKey] + '<br>';
          }
        }
        else{
          this.mensaje = error.error.message;
        }
      }

      /* =======  End of La respuesta no es correcta  ======= */
      );

    }else{

      alert('Favor de colocarle una contraseña');

    }

  }

}
