import { Component, OnInit } from '@angular/core';
import { UsuariosModel } from '../../models/usuarios.model';
import { SucursalesModel } from '../../models/sucursales.model';
import { UsuarioService } from '../../services/usuario.service';
import { SucursalService } from '../../services/sucursal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edita-usuario',
  templateUrl: '../crear-usuario/crear-usuario.component.html',
  styleUrls: ['./edita-usuario.component.css'],
  providers: [SucursalesModel, UsuariosModel]
})
export class EditaUsuarioComponent implements OnInit {

  public title: string;
  public breadcum: Array<string>;
  public rutaBreadcum: string;
  public status: string;
  public mensaje: string;
  public token: string;

  constructor(
    public usuario: UsuariosModel,
    public sucursales: SucursalesModel,
    private usuarioService: UsuarioService,
    private sucursalService: SucursalService,
    private router: Router,
    private route: ActivatedRoute

  ) {

    this.title = 'Edita Usuario';
    this.breadcum = ['Usuarios', 'edita-usuario'];
    this.rutaBreadcum = '/usuarios';
    this.token = this.usuarioService.getToken();
    this.getSucursales();
   }

  ngOnInit(): void {
  }

  // Obtenemos los datos de las sucursales para llenar el combo sucursal
  getSucursales(){
    this.sucursalService.getSucursales(this.token).subscribe(
      response => {
        if (response.status === 'success'){
          this.sucursales = response.sucursales;
          // si sale todo bien se traen los datos del usuario.
          this.traerUsuario();
        }
        else{
          console.log(response);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  // Obtenemos los datos del usuario por medio de la url
  traerUsuario(){

    this.route.params.subscribe(
      params => {
        const id = params.id;
        this.usuarioService.getUsuario(this.token, id).subscribe(
          response => {
            if (response.status === 'success'){
              this.usuario = response.user;
            }else{
              console.log(response);
              this.status = 'error';
              this.mensaje = response.message;
            }
          },
          error => {
            console.log(error);
            this.status = 'error';
            this.mensaje = error.error.message;
          }
        );
      }
    );

  }

  onSubmit(form: NgForm){
    if (form.invalid) { return; }

    this.usuarioService.editaUsuario(this.usuario, this.token, this.usuario.id_usuario).subscribe(

      /* ================================================== *
       * ==========  Respuesta correcta  ========== *
       * ================================================== */
      response => {
        if (response.status === 'success'){
          this.status = 'success';
          this.mensaje = response.message;
          this.router.navigate(['/usuarios']);
        }else{
          console.log(response);
          this.status = 'error';
          this.mensaje = response.message;
        }
      },
      /* =======  End of Respuesta correcta  ======= */

      /* ================================================== *
       * ==========  Respuesta con Error  ========== *
       * ================================================== */
       error => {
         console.log(error);
         this.status = 'error';

         if ( error.error.errors === undefined){// aqui definimos no hay errores en los campos
           this.mensaje = error.error.message;
         }
         else{ // aqui se explica los errores llenados en los campos no son correctos
          this.mensaje = '';
          // tslint:disable-next-line: forin
          for (const keyError in error.error.errors){
            this.mensaje += error.error.errors[keyError] + '<br>';
          }
         }
       }
      /* =======  End of Respuesta con Error  ======= */
    );

  }



}
