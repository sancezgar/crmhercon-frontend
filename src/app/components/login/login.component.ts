import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuariosModel } from '../../models/usuarios.model';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public usuario: UsuariosModel;
  public status: string;

  constructor(
    private usuarioService: UsuarioService
  ) {
    this.usuario = new UsuariosModel();
  }

  ngOnInit(): void {
  }

  login(form: NgForm){

    if (form.invalid) { return; }

    // console.log(this.usuario);

    this.usuarioService.login(this.usuario).subscribe(
      response => {
        if ( response.status ){

          console.log(response);
          this.status = 'error';

        }else{

          const token = response;
          localStorage.setItem('token', token);

          this.usuarioService.login(this.usuario, 'true').subscribe(
            resp => {

              const datos = JSON.stringify(resp);
              localStorage.setItem('identity', datos);

            },
            error => {
              console.log(error);
              this.status = 'error';
            }
          );

        }
      },
      error => {
        console.log(error);
        this.status = 'error';
      }
    );
  }

}
