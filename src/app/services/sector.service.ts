import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { global } from './global';
import { SectorModel } from '../models/sector.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SectorService {
  private url: string;
  constructor(

    private http: HttpClient

  ) {

    this.url = global.url;

  }

  getSectores(token: string): Observable<any>{

    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.get(this.url + 'sector', {headers});

  }

  getSector(token: string, id: number): Observable<any>{

    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.get(this.url + 'sector/' + id, {headers});

  }

  creaSector(sector: SectorModel, token: string): Observable<any>{

    const json = JSON.stringify(sector);
    const params = 'json=' + json;

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                     .set('Authorization', token);
    return this.http.post(this.url + 'sector', params, {headers});

  }

  editaSector(sector: SectorModel, token: string, id: number): Observable<any>{

    const json = JSON.stringify(sector);
    const params = 'json=' + json;

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                     .set('Authorization', token);
    return this.http.put(this.url + 'sector/' + id, params, {headers});

  }

  eliminaSector(token: string, id: number): Observable<any>{

    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.delete(this.url + 'sector/' + id, {headers});

  }
}
