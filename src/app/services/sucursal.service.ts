import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { global } from './global';
import { Observable } from 'rxjs';
import { SucursalesModel } from '../models/sucursales.model';

@Injectable({
  providedIn: 'root'
})
export class SucursalService {

  private url: string;

  constructor(
    private http: HttpClient
  ) {

    this.url = global.url;

  }

  public getSucursales( token: string): Observable<any>{

   const headers = new HttpHeaders().set('Authorization', token);
   return this.http.get(this.url + 'sucursal', {headers});

  }

  public crearSucursal(sucursal: SucursalesModel, token: string): Observable<any>{

    const json = JSON.stringify(sucursal);
    const params = 'json=' + json;

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                      .set('Authorization', token);

    return this.http.post(this.url + 'sucursal', params, {headers});

  }

  public editarSucursal(sucursal: SucursalesModel, token: string, id: number): Observable<any>{

    const json = JSON.stringify(sucursal);
    const params = 'json=' + json;

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                     .set('Authorization', token);

    return this.http.put(this.url + 'sucursal/' + id, params, {headers});

  }

  public getSucursal(token: string, id: number): Observable<any>{

    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.get(this.url + 'sucursal/' + id, {headers});

  }

  public eliminarSucursal(token: string, id: number): Observable<any>{

    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.delete(this.url + 'sucursal/' + id, {headers});

  }


}
