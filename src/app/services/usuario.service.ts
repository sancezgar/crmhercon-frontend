import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { global } from './global';
import { Observable } from 'rxjs';
import { UsuariosModel } from '../models/usuarios.model';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  public url: string;

  constructor(
    private http: HttpClient
  ) {

    this.url = global.url;

  }

  public login(usuario: UsuariosModel, getToken: string = null): Observable<any>{

    if (getToken) { usuario.getToken = getToken; }
    const json = JSON.stringify(usuario);
    const params = 'json=' + json;

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    return this.http.post(this.url + 'login', params , {headers});

  }

  public revisarToken(token: string): Observable<any>{

    const params = 'token=' + token;

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    return this.http.post(this.url + 'revisarToken', params, {headers});

  }

  public getUsuarios(token: string): Observable<any>{
    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.get(this.url + 'user', {headers});
  }

  public getUsuario(token: string, id: number): Observable<any>{
    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.get(this.url + 'user/' + id, {headers} );
  }

  public crearUsuario(usuario: UsuariosModel, token: string): Observable<any>{

    const json = JSON.stringify(usuario);
    const params = 'json=' + json;

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                     .set('Authorization', token);
    return this.http.post(this.url + 'user', params, {headers});

  }

  public editaUsuario(usuario: UsuariosModel, token: string, id: number): Observable<any>{

    const json = JSON.stringify(usuario);
    const params = 'json=' + json;

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                      .set('Authorization', token);
    return this.http.put(this.url + 'user/' + id, params, {headers});
  }

  public eliminaUsuario(token: string, id: number): Observable<any>{

    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.delete(this.url + 'user/' + id, {headers});

  }

  public getIdentity(){

    const identity = localStorage.getItem('identity');
    return JSON.parse(identity);

  }

  public getToken(){
    return localStorage.getItem('token');
  }


}
