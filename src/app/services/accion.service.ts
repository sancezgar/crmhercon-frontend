import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { global } from './global';
import { Observable } from 'rxjs';
import { AccionesModel } from '../models/acciones.model';

@Injectable({
  providedIn: 'root'
})
export class AccionService {
  private url: string;
  constructor(
    private http: HttpClient
  ) {

    this.url = global.url;

  }

  getAcciones(token: string): Observable<any>{

    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.get(this.url + 'accion', {headers});

  }

  getAccion(token: string, id: number): Observable<any>{

    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.get(this.url + 'accion/' + id, {headers});

  }

  creaAccion(accion: AccionesModel, token: string): Observable<any>{

    const json = JSON.stringify(accion);
    const params = 'json=' + json;

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                     .set('Authorization', token);
    return this.http.post(this.url + 'accion', params, {headers});

  }

  editaAccion(accion: AccionesModel, token: string, id: number): Observable<any>{

    const json = JSON.stringify(accion);
    const params = 'json=' + json;

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
                                     .set('Authorization', token);
    return this.http.put(this.url + 'accion/' + id, params, {headers});

  }

  eliminaAccion(token: string, id: number): Observable<any>{

    const headers = new HttpHeaders().set('Authorization', token);
    return this.http.delete(this.url + 'accion/' + id, {headers});

  }

}
