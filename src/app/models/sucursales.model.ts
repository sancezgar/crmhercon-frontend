export class SucursalesModel{

    // tslint:disable-next-line: variable-name
    public id_sucursal: number;
    // tslint:disable-next-line: variable-name
    public razon_social: string;
    // tslint:disable-next-line: variable-name
    public rfc_sucursal: string;
    public alias: string;

}