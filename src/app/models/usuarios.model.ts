export class UsuariosModel{

    // tslint:disable-next-line: variable-name
    public id_usuario: number;
    // tslint:disable-next-line: variable-name
    public nombre_usuario: string;
    public rol: string;
    // tslint:disable-next-line: variable-name
    public email_usuario: string;
    public contrasena: string;
    // tslint:disable-next-line: variable-name
    public id_sucursal: number;
    public getToken: string;
    public sucursal: any;


}
