import { Component, DoCheck } from '@angular/core';
import { UsuarioService } from './services/usuario.service';
import { UsuariosModel } from './models/usuarios.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements DoCheck{
    token: string;
    identity: object;
    usuario: UsuariosModel;

  constructor(
    private usuarioService: UsuarioService,
    private router: Router
  ){
    this.token = this.usuarioService.getToken();
    this.identity = this.usuarioService.getIdentity();
    setTimeout(() => {
      if (this.token !== null){
        this.revisarSesion(this.token);
      }
    }, 500);
  }

  ngDoCheck(){
    this.token = this.usuarioService.getToken();
    this.identity = this.usuarioService.getIdentity();
  }

  revisarSesion(token: string){
    this.usuarioService.revisarToken(token).subscribe(
      response => {
        if (!response.sesion){
          this.router.navigate(['/inicio', 1]);
        }
      },
      error => {
        console.log(error);
      }
    );

  }


}
